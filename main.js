const compiler = require('./utils/compiler');

const path = require('path');
const express = require('express');
const app = express();

const port = 5000;
const indexRouter = require('./backend/controllers/root');

compiler(path.resolve(__dirname, 'contractsSol/EscrowTest.sol'), path.resolve(__dirname, 'contractsJson/EscrowTest.json'), (err)=>{
    if(err) console.log("Error compiling contract! \n" + err);
    else console.log("Contract compiled successfully!");
});

app.set('views', path.resolve(__dirname, 'frontend/views'));
app.set('view engine', 'pug');

app.use('/', indexRouter);

app.listen(port, ()=> console.log(`Server listening on port ${port}`));