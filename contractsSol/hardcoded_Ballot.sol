pragma solidity >= 0.5.0 < 0.7.0;

contract Ballot {
  struct Votante{
    uint8 peso;
    address delegado;
    bool voto;
    uint8 porQuien;
  }

  struct Candidato{
    string nombre;
    uint votos;
  }

  address moderador;
  mapping(address => Votante) votantes;
  Candidato[] candidatos;

  constructor() public {
    moderador = msg.sender;
  }

  function agregarCandidato(string memory nombre) public {
    require(msg.sender == moderador, "Only the moderator has the ability to add a Candidate");
    candidatos.push(Candidato({nombre: nombre, votos: 0}));
  }

  function getConteoCandidatos() public view returns(uint){
    return candidatos.length;
  }

  function getCandidatoPorIndex(uint8 index) public view returns(string memory, uint){
    return (candidatos[index].nombre, candidatos[index].votos);
  }

  function getCandidatoGanador() public view returns(string memory, uint){
    uint indice = 0;
    uint votosMax = 0;
    for(uint i = 0; i < candidatos.length; i++){
      if(candidatos[i].votos > votosMax){
        indice = i;
        votosMax = candidatos[i].votos;
      }
    }
    return (candidatos[indice].nombre, candidatos[indice].votos);
  }

  function votar(uint8 indice) public {
    Votante storage sender = votantes[msg.sender];
    require(sender.peso != 0, "The current account has no right to vote (yet)");
    require(!sender.voto, "The current account already issued a vote");
    uint newIndex = indice;
    require(keccak256(abi.encodePacked(candidatos[newIndex].nombre)) != "", "The selected candidate option is not valid");
    candidatos[newIndex].votos += sender.peso;
    sender.voto = true;
    sender.porQuien = indice;
  }

  function delegar(address aQuien) public{
    Votante storage sender = votantes[msg.sender];
    address to = aQuien;
    require(sender.peso != 0, "The current account has no right to vote");
    require(!sender.voto, "The current account already issued a vote so it cannot delegate");
    while(votantes[to].delegado != address(0)){
      to = votantes[to].delegado;
      require(to != msg.sender, "A delegation loop was found; not allowed");
    }
    require(votantes[to].peso != 0, "The target account has no right to vote");
    sender.voto = true;
    sender.delegado = to;
    if(votantes[to].voto) candidatos[votantes[to].porQuien].votos += sender.peso;
    else votantes[to].peso += sender.peso;
  }

  function habilitarCuenta(address cuenta) public{
    require(msg.sender == moderador, "The only person that can enable an account to vote is the moderator");
    require(votantes[cuenta].peso == 0, "The selected account is already enabled to vote");
    require(!votantes[cuenta].voto, "The selected account already issued a vote");
    votantes[cuenta].peso = 1;
  }
}
