pragma solidity >= 0.5.0 < 0.7.0;

contract EscrowTest {
  uint estipulatedAmount = 1;
  address payable worker;
  address payable contractor;
  address payable escrow;
  uint fineCollector;
  uint pendingPayment;
  bool locker;

  constructor(address payable _contractor, address payable _worker, address payable _escrow) public {
    escrow = escrow;
    contractor = _contractor;
    worker = _worker;
    locker = true;
  }

  modifier escrowOnly(){
    require(msg.sender == escrow, "This operation can only be executed by the Escrow account");
    _;
  }

  modifier workerOnly(){
    require(msg.sender == worker, "This operation can only be carried through by the current worker");
    _;
  }

  modifier contractorOnly{
    require(msg.sender == contractor, "This operation can only be performed by the contractor");
    _;
  }

  event paymentWithdrawn(uint withdrawnQuantity);

  function concatenate(string memory a, string memory b) internal pure returns (string memory){
    bytes memory _a = bytes(a);
    bytes memory _b = bytes(b);
    string memory temp = new string(_a.length + _b.length);
    bytes memory retorno = bytes(temp);
    uint16 k = 0;
    for(uint i = 0; i < _a.length; i++) retorno[k++] = _a[i];
    for(uint i = 0; i < _b.length; i++) retorno[k++] = _b[i];
    return string(retorno);
  }

  function uint2str(uint _i) internal pure returns(string memory){
    uint origin = _i;
    if(origin == 0){
      return "0";
    }

    uint j = origin;
    uint len;
    while(j != 0){
      len++;
      j /= 10;
    }
    bytes memory bstr = new bytes(len);
    uint k = len - 1;
    while(origin != 0){
      bstr[k--] = byte(uint8(48 + origin % 10));
      origin /= 10;
    }
    return string(bstr);
  }

  function setWorker(address payable newWorker) public escrowOnly{
    worker = newWorker;
  }

  function withdrawPayment() public payable workerOnly{
    if(pendingPayment != 0){
      uint amount = pendingPayment;
      pendingPayment = 0;
      (bool success, ) = msg.sender.call.value(amount)("");
      if(!success) pendingPayment = amount;
      else emit paymentWithdrawn(amount);
    }
  }

  function depositPayment() public payable contractorOnly{
    if(locker){
      locker = false;
      string memory temp = concatenate("The estipulated amount and received amount do not match", uint2str(msg.value));
      require(msg.value == estipulatedAmount * 1000000000000000000, temp);
      pendingPayment += msg.value;
      locker = true;
    }
  }

  function setEstipulatedAmount(uint _estipulated) public contractorOnly{
    estipulatedAmount = _estipulated;
  }

  function refundPendingPayments() public escrowOnly{
    uint amount = pendingPayment;
    if(amount > 0){
      pendingPayment = 0;
      uint tempFine = uint(amount * 3 / 100);
      uint newAmount = amount - tempFine;
      fineCollector += tempFine;
      (bool success, ) = contractor.call.value(newAmount)("");
      if(!success){
        pendingPayment = newAmount;
        fineCollector -= tempFine;
      }
    }
  }

  function checkBalance() public view escrowOnly returns(uint){
    return pendingPayment;
  }

}
