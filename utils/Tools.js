const NewDateHandler = {
    get(obj, prop){
        if(prop == "isoDate"){
            let formated = "";
            formated += obj.getFullYear();
            formated += '-';
            formated += (obj.getMonth() + 1);
            formated += '-';
            formated += obj.getDate();
            return formated;
        } else {
            return obj[prop];
        }
    }
};

const newDateInstance = () =>{
    let dateInst = new Date();
    return new Proxy(dateInst, NewDateHandler);
};

module.exports = {
    newDateInstance
};