const Tools = require('./Tools');

const https = require('https');
const FS = require('fs');
const path = "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD";

const NuevaFecha = Tools.newDateInstance();

/** 
 * Get the current ETH to USD exchange
 * @param {string} configPath - Relative path from the caller to the config.json file
 * @param {function} onResult - Callback function that receives the current exchange as a number. 
*/
const getExchange = (configPath, onResult) => {
    FS.readFile(configPath, (err, data)=>{
        if(err) console.error(err);
        const config = JSON.parse(data);
        let resultado = {
            value: config.exchangeEtherUsd.value,
            date: config.exchangeEtherUsd.date
        };
        let resObj = undefined;
        https.get(path, (response)=>{
            let buffer = '';
            response.on('data', (datos) => {
                buffer += datos;
            });

            response.on('end', ()=>{
                resObj = JSON.parse(buffer);
                if(typeof(resObj['USD'] == 'number')){
                    resultado = {
                        value: resObj['USD'],
                        date: NuevaFecha.isoDate
                    };
                    config.exchangeEtherUsd = resultado;
                    FS.writeFile(configPath, JSON.stringify(config), (err)=>{
                        if(err) console.error(err);
                    });
                }
                onResult(resultado);
            });
        }).on('error', (error)=>{
            console.error(error);
        });
    });
};

module.exports = getExchange;