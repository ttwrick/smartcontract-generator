const Tools = require('./Tools');

const Web3 = require('web3');
const FileSystem = require('fs');
const path = require('path');
const getExchange = require('./GetExchange');

/**
 * A class for basic deployment and interaction with contracts
 */
module.exports = class ContractHandler {
    /**
     * Create a ContractHandler instance
     * @param {String} compiledContractPath - Path for the contracts json after compilation
     * @param {String} contractName - Name of the contract
     * @param {String} configPath - [Optional] Path for the configuration file
     * @param {Array} parameters - [Optional] An array of the parameters that the contract should take
     */
    constructor(compiledContractPath, contractName, configPath = 'config.json', parameters = []){
        let newDate = Tools.newDateInstance();
        this.config = JSON.parse(FileSystem.readFileSync(configPath));
        // The next line should change depending of the way the blockchain is being simulated:
        // this.config.devProviderCLI for the command line tool and this.config.devProviderGUI for the ganache GUI
        let web3 = new Web3(this.config.devProviderGUI); // TODO: This should be modifiable in some way, not hardcoded
        let contractBin = JSON.parse(FileSystem.readFileSync(compiledContractPath));
        let baseName = contractName + '.sol';
        this.jsonContract = contractBin.contracts[baseName][contractName];
        this.contract = new web3.eth.Contract(this.jsonContract.abi);
        this.contractDeployment  = this.contract.deploy({
            data: '0x' + this.jsonContract.evm.bytecode.object,
            arguments: parameters
        });
        this.isContractDeployed = false;
        this.contractInstance = undefined;
        this.averageWeiGasPrice = undefined;
        this.contractEtherGasPrice = undefined;
        this.contractWeiGasPrice = undefined;
        this.etherToUsd = undefined;

        web3.eth.getGasPrice((err, gasPrice)=>{
            if(err) console.error(err);
            this.averageWeiGasPrice = gasPrice;
            this.config.exchangeGasWei = {
                value: this.averageWeiGasPrice,
                date: newDate.isoDate
            };
            this.contractDeployment.estimateGas((problem, gasNumber)=>{
                if(problem) console.error(problem);
                this.contractWeiGasPrice = gasNumber * this.averageWeiGasPrice;
                this.contractEtherGasPrice = web3.utils.fromWei(this.contractWeiGasPrice.toString(), "ether");
                getExchange(configPath, result => this.etherToUsd = result);
            });
        });
    }

    /**
     * Get the estimated cost for deployment in Ether
     * @returns a number representing the deployment cost in ETH
     */
    getEthContractValue(){
        if(this.contractEtherGasPrice) return this.contractEtherGasPrice;
        else return this.config.exchangeGasWei * this.config.exchangeWeiEther;
    }

    /**
     * Get the estimated cost for deployment in US dollars
     * @returns a number representing the deployment cost in USD
     */
    getUsdContractValue(){
        if(this.etherToUsd) return Math.round(this.getEthContractValue() * this.etherToUsd.value * 100) / 100.0;
        else return Math.round(this.getEthContractValue() * this.config.exchangeEtherUsd.value * 100) / 100.0;
    }

    /**
     * @callback newContractCB
     * @param {object} newContractInstance - The instace of the deployed contract
     */
    /**
     * Deploy the current contract to the blockchain and calls the contractInstance(newContractInstance)
     * @param {string} currentAccountAddress - The address from which the deployment originates; this address will have to pay for deployment and therefore requires to be unlocked.
     * @param {newContractCB} contractInstanceCB - Callback function that will receive the new instance of the deployed contract as parameter.
     */
    deployContract(currentAccountAddress, contractInstanceCB){
        if(this.isContractDeployed) return;
        this.contractDeployment.send({
            from: currentAccountAddress,
            gas: 1500000
        }).then((newContractInstance) => {
            this.isContractDeployed = true;
            this.contractInstance = newContractInstance;
            contractInstanceCB(newContractInstance);
        });
    }

    /**
     * Get the current contract's instance if the contract has been deployed
     * @returns the contract's instance or false if not yet deployed
     */
    getContractInstance(){
        if(!this.isContractDeployed) return false;
        else return this.contractInstance;
    }
}