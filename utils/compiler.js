const SolC = require('solc');
const FS = require('fs');
const path = require('path');

/**
 * Compile a solidity contract using the solc module
 * @param {String} inputSol - Path to a solidity contract file
 * @returns The compiled JSON
 */
const chew = (inputSol) => {
    let inputContent = undefined;
    if(!FS.existsSync(inputSol)) return false;
    inputContent = FS.readFileSync(inputSol, 'utf8');
    let basename = path.basename(inputSol);
    let config = {
        language: 'Solidity',
        sources: {},
        settings: {
            outputSelection: {
                '*': {
                    '*': ['*']
                }
            }
        }
    };
    config.sources[basename] = { content: inputContent};

    if(!config.sources[basename].content) return false;

    let output = undefined;
    
    try{
        output = JSON.parse(SolC.compile(JSON.stringify(config)));
    } catch(error){
        return false;
    }

    return output;
};
/**
 * @callback compileCB
 * @param {object} error - An object undefined unless an error occurs
 */
/**
 * Compile a solidity contract into a new JSON file
 * @param {String} inputPath - Path to the solidity contract file
 * @param {String} outputPath - Path for the JSON output to be written in
 * @param {compileCB} callback - A callback that will be called when the compile process finishes
 */
const compile = (inputPath, outputPath, callback) => {
    let outputData = chew(inputPath);
    if(!outputData) throw error("The compilation process could not finish!");
    else {
        outputData = JSON.stringify(outputData);
        FS.writeFile(outputPath, outputData, callback);
    }
};

module.exports = compile;