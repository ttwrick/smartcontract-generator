# Smart contract generator

## Introduction

Welcome! This project is supposed to be a prototype, so there is not much to say other than the main purpose: to deploy a Smart Contract through a web application that allows the user to input the parties involved.

## How to run the project

To be able to run the code within this project the first thing is to install Node.js and npm (if they're not already installed). Then, the next requirement can be **Ganache** or **ganache-cli**; the difference between the two being that the former has GUI while the latter does not.

To install **Ganache** go to the [Truffle suite site](https://www.trufflesuite.com/) and follow the instructions. After installing, running the app is as simple as opening it and clicking on the "Quickstart" button.

The **ganache-cli** on the other hand, needs to be executed from a terminal by typing its name as a command (just typing `ganache-cli`). To install **ganache-cli** run the following command in a terminal:

`npm install -g ganache-cli`

Once installed, run the program selected in background while testing the project.

To test the project, clone the repository and execute the following commands from the root:

```
npm install
npm run start
```
**Note that a ganache implementation must be running in the background for the project to work**

**Depending on the ganache version selected, line 24 in 'ContractHandler.js' might need updating:** if using Ganache (GUI), the passed argument to the web3 constructor needs to be changed from _this.config.devProviderCLI_ to _this.config.devPrividerGUI_ for the test to run.

### Previously expected result

The project should create a new folder called _contractsJson_ and within said folder a file with the JSON resulting from the compilation of the hardcoded contracts included in the _contractsSol_ folder. Then, in the terminal the output should calculate an estimate of the ether it would cost to deploy said contract by current average gas prices.

Edit 1: After the aforementioned, the EscrowTest contract should be deployed and two transactions must occur (a deposit and a withdrawal). Also, the config.json file should update on running.

### Current expected result
Once the project installs and runs, a server will start listening to the URL http://localhost:5000.
Completing the form with valid information should lead to the creation of a smart contract between the inputted parties.
Finally, the site will redirect to a page showing the address at which the contract was deployed and a list of available interactions. 
The app does **not** provide a way to interact with the created contract.
**The expected results of running the project will greatly vary as the development continues**