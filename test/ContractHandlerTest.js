const ContractHandler = require('../utils/ContractHandler');
const FS = require('fs');
const path = require('path');
const compiler = require('../utils/compiler');

console.log("Starting compile process..");
const outputFolder = '../contractsJson';
const inputFolder = '../contractsSol';
let exists = FS.existsSync(path.resolve(__dirname, outputFolder));
if(!exists) FS.mkdirSync(path.resolve(__dirname, outputFolder));
else console.log("The output folder exists!");

const outputFile = path.resolve(__dirname, outputFolder + '/EscrowTest.json');
const inputFile = path.resolve(__dirname, inputFolder + '/EscrowTest.sol');
let contractname = "EscrowTest";
let configPath = path.resolve(__dirname, '../config.json');

const onOutput = (error) =>{
    if(error) console.error(error);
    console.log("Compilation process finished!");
    let handler = new ContractHandler(outputFile, contractname, configPath, ['0xfF2E6118847e61E330B59aFf93D5ed46a44c63d5',
    '0xb4587F80ca3873ac9D0FFb73533866fBeC001D41', '0x90b5d7cbA92481D9b9e031018CFc758a34174951']);
    setTimeout(()=>{
        console.log(`avg WEI gas: ${handler.averageWeiGasPrice}`);
        console.log(`ctrct WEI price: ${handler.contractWeiGasPrice}`);
        console.log(`ethToUsd: ${JSON.stringify(handler.etherToUsd)}`);
        console.log(`ETH value: ${handler.getEthContractValue()}`);
        console.log(`USD value: ${handler.getUsdContractValue()}`);
        handler.deployContract('0xfF2E6118847e61E330B59aFf93D5ed46a44c63d5', (instance)=>{
            console.log("Contract allegedly deployed!");
            console.log("Instance address: " + instance.options.address);
            instance.methods.setEstipulatedAmount('1').send({from: '0xfF2E6118847e61E330B59aFf93D5ed46a44c63d5'})
            .then((receiptA)=>{
                console.log("Estipulated amount set to 1");
                instance.methods.depositPayment().send({from: '0xfF2E6118847e61E330B59aFf93D5ed46a44c63d5', 
                value: '1000000000000000000'}).then((receiptB)=>{
                    console.log("Deposit made");
                    instance.methods.withdrawPayment().send({from: '0xb4587F80ca3873ac9D0FFb73533866fBeC001D41'})
                    .then((receiptC)=>{
                        console.log("Withdrawal made");
                    });
                });
            });
        });
    }, 2 * 1000);
};

compiler(inputFile, outputFile, onOutput);