const uuidv4 = require('uuid/v4');
const express = require('express');
const router = express.Router();
const session = require('express-session');

const path = require('path');

const ContractHandler = require('../../utils/ContractHandler');
const escrowAddress = '0x90b5d7cbA92481D9b9e031018CFc758a34174951'; //This should change in accordance to test env

router.use(session({
    genid: function(){
        return uuidv4();
    },
    secret: 'tres tristes tigres',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 2*60*1000
    }
}));

router.use(express.static(path.resolve(__dirname, '../../frontend/css')));
router.use(express.static(path.resolve(__dirname, '../../frontend/javascript')));

router.use(express.json());
router.use(express.urlencoded({extended: true}));

router.get('/', (req, res, next)=>{
    if(req.session.contract){
        res.redirect(301, '/manageContract');
    } else {
        res.render('index', {
            title: 'Welcome',
            header: 'Hello world!'
        });
    }
});

router.post('/createContract', (req, res, next)=>{
    if(req.session.contractAddress) { 
        res.redirect(301, '/manageContract');
    }
    else {
        let contract = new ContractHandler(
            path.resolve(__dirname, '../../contractsJson/EscrowTest.json'), "EscrowTest",
            path.resolve(__dirname, '../../config.json'),
            [req.body.contractorAddress, req.body.workerAddress, escrowAddress]
        );

        contract.deployContract(req.body.contractorAddress, (newContract)=>{
            req.session.contractAddress = newContract.options.address;
            res.render('createContract', {
                title: "Contract creator",
                contractor: req.body.contractorName,
                contractorAddress: req.body.contractorAddress,
                worker: req.body.workerName,
                workerAddress: req.body.workerAddress
            });
        });
    }
});

router.get('/manageContract', (req, res, next)=>{
    if(!req.session.contractAddress){
        res.render('contractNeeded', {
            title: "Need a contract to be here"
        });
    } else {

        res.render('manageContract', {
            title: 'Contract manager',
            contractAddress: req.session.contractAddress
        });
    }
});

router.get('/about', (req, res, next)=>{
    res.render('about', {
        title: 'About',
        header: 'Smart Escrow Prototype',
        subheader: 'Ricardo José Guevara Aragón',
        info: '2020'
    });
});

module.exports = router;